import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyMapComponent } from './components/my-map/my-map.component';
import { VisualizemapComponent } from './components/visualizemap/visualizemap.component';

const routes: Routes = [
  { path: 'map', component: MyMapComponent },
  { path: 'visualize', component: VisualizemapComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
