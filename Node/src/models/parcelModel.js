const {DataTypes} = require('sequelize');
const sequelize = require('../config/db').sequelize;

module.exports.parcelModel=sequelize.define('parcel',{
    id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    parcelname: {
        type: DataTypes.STRING,
        allowNull: true
      },
      area: {
        type: DataTypes.DOUBLE,
        allowNull:true
      }
},{
    freezeTableName:true,
    timestamps:false
});


