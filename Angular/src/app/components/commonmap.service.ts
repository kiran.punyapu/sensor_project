import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class CommonmapService {

  constructor(private http: HttpClient) { }

  createParcel(parceldata): Observable<any> {
    return this.http.post(
      "http://localhost:8000/api/parcel/createparcel",
      parceldata,{responseType: 'text'}
    );
  }

  getparcels():Observable<any>{
    return this.http.get(
      "http://localhost:8000/api/parcel/getall"
    );
  }
  getparcelsbyid(data):Observable<any>{
    return this.http.get(
      "http://localhost:8000/api/parcel/getparbyid/"+ data
    );
  }
  getGeobyid(data):Observable<any>{
    return this.http.get(
      "http://localhost:8000/api/parcel/getGeoLocation/"+ data
    );
  }

  getallgeolocations():Observable<any>{
    return this.http.get(
      "http://localhost:8000/api/parcel/findallgeo"
    );
  }
  
}
