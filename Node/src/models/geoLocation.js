const {DataTypes} = require('sequelize');
const sequelize = require('../config/db').sequelize;

module.exports.locationModel=sequelize.define('geolocation',{
    id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    parcelid:{
        type:DataTypes.INTEGER
    },
    lat: {
        type: DataTypes.DECIMAL,
        allowNull: true
      },
      lng: {
        type: DataTypes.DECIMAL,
        allowNull:true
      }
},{
    freezeTableName:true,
    timestamps:false
});


