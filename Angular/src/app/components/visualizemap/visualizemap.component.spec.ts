import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizemapComponent } from './visualizemap.component';

describe('VisualizemapComponent', () => {
  let component: VisualizemapComponent;
  let fixture: ComponentFixture<VisualizemapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizemapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizemapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
