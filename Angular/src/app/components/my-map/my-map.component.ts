import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MapLoaderService } from "./mymaploader";
declare var google: any;
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import {CommonmapService} from '../commonmap.service';

@Component({
  selector: 'app-my-map',
  templateUrl: './my-map.component.html',
  styleUrls: ['./my-map.component.scss']
})
export class MyMapComponent implements OnInit, AfterViewInit {
  map: any;
  retrivemap:any;
  drawingManager: any;
  showmodel:Boolean=false
  parcelFrom: FormGroup;
  polygoncoord:any;
  polymap:Boolean=false
  infoWindow;
  parceldtls:Array<any> = [];
  data:Array<any> = [];
  object:Array<any> = [];
  constructor(private router:Router,private mapservice:CommonmapService) { }

  ngOnInit() {
    this.parcelFrom = new FormGroup({
      parcelname: new FormControl(''),
      area: new FormControl('')
    });

   
  this.getparcels();
   
  }

  getparcels(){
    this.mapservice.getparcels().subscribe(response=>{
      console.log(response)
      this.parceldtls=JSON.parse(JSON.stringify(response))
    })
  }

  ngAfterViewInit() {
    MapLoaderService.load().then(() => {
      this.drawPolygon();
    });

  }
  drawPolygon() {
    this.map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 8
    });
    this.drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: true,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: ["polygon"]
      }
    });
    this.drawingManager.setMap(this.map);
    google.maps.event.addListener(
      this.drawingManager,
      "overlaycomplete",
      event => {
        if (event.type === google.maps.drawing.OverlayType.POLYGON) {
          this.polygoncoord=JSON.stringify(event.overlay.getPath().getArray())
          console.log(this.polygoncoord)
          // localStorage.setItem('coordinates',this.polygoncoord)
          let areainhec = google.maps.geometry.spherical.computeArea(event.overlay.getPath().getArray())/10000
          console.log(google.maps.geometry.spherical.computeArea(event.overlay.getPath().getArray()));
          console.log(areainhec);
          this.showmodel=true
          this.parcelFrom.controls['area'].setValue(areainhec)
        }
      }
    );
  }

  createparcel(){
    console.log(this.parcelFrom.value)
    const parceldata={
      "parcelentry":this.parcelFrom.value,
      "location":this.polygoncoord
    }
    this.mapservice.createParcel(parceldata).subscribe(res=>{
      if(res!=undefined && res!=null){
      console.log(JSON.stringify(res))
        this.getparcels();
      }
    })
    // this.router.navigate(['/visualize'])
  }

  viewlocation(index){
    console.log(this.parceldtls[index].id)
    this.mapservice.getGeobyid(this.parceldtls[index].id).subscribe(res=>{
    this.data = res
    console.log(this.data)
    let result = this.data.map((elem) => ({
      'lat': elem.lat,
      'lng': elem.lng
    }))
    let newresult = result.map(entry => 
      Object.entries(entry).reduce(
          (obj, [key, value]) => (obj[key] = parseFloat(value), obj), 
          {}
      )
    );
     this.mapservice.getparcelsbyid(this.data[0].parcelid).subscribe(response=>{
       localStorage.setItem('parcelname',response[0].parcelname)
       localStorage.setItem('area',response[0].area)
     })
 this.polygoncoord = JSON.stringify(newresult)
  localStorage.setItem('coordinates',this.polygoncoord)
  localStorage.setItem('area',this.polygoncoord)
  this.router.navigate(['/visualize'])
    })
  }


}
