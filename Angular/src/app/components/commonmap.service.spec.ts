import { TestBed } from '@angular/core/testing';

import { CommonmapService } from './commonmap.service';

describe('CommonmapService', () => {
  let service: CommonmapService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonmapService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
