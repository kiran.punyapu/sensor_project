const express = require('express');
const router = express.Router();
const UserModel = require('../models/UserModel').UserModel;
const { comparePassword, toHash } = require('../utilities/Password');
const { jwtSign,verifyToken } = require('../utilities/auth');

router.post('/signup',async (req,res)=>{
    req.body.password = await toHash(req.body.password);
    const response = await UserModel.create(req.body);
    res.status(201).send(response);
});



router.post('/signin',async (req,res)=>{
    const { emailId, password} = req.body;
    const user = await UserModel.findOne({where:{emailId:emailId}});
    if(!user) return res.status(200).send({message:"User Not found"});
    const userExists = user.get({plain:true});
    const isPassword = await comparePassword(userExists.password,password);
    if(!isPassword) return res.status(200).send({message:"Invalid Password"});
    let session=req.session;
    session.jwt= jwtSign(userExists)
    res.status(200).send({token:jwtSign(userExists)});  
 });

// router.get('/me',(req,res)=>{
//     console.log(req.session.jwt)
//         if(!req.session.jwt)
//         return res.status(500).send({message:"Invalid user"});
//             res.status(200).send(verifyToken(req.session.jwt))  
// });

router.get('/getall',async (req,res)=>{
    res.status(200).send(await UserModel.findAll())
});

router.post("/getbyemail",async(req,res)=>{
   res.status(200).send(await (await UserModel.findOne({where:{emailId:req.body.emailid}})).get({plain:true}))
});
router.post("/deleteuser",async(req,res)=>{
    await UserModel.destroy({where:{id:req.body.id}});
    res.status(200).send({message:`User with ${req.body.id} deleted successfully`});
})

module.exports=router;