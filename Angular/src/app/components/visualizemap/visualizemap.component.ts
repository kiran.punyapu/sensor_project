import { Component, OnInit,ViewChild  } from '@angular/core';
import { MapLoaderService } from "../my-map/mymaploader";
declare var google: any;
import {CommonmapService} from '../commonmap.service';
@Component({
  selector: 'app-visualizemap',
  templateUrl: './visualizemap.component.html',
  styleUrls: ['./visualizemap.component.scss']
})
export class VisualizemapComponent implements OnInit {
  @ViewChild("map", { static: true }) mapElement: any;
  map: any;
  latitude: number;
  longitute: number;
  coordinates:any;
  data:Array<any> = [];
  dammycoordinates:any;
  dammymap:any;
  drawingManager: any;
  parcelarea:any;
  parcelname:any;
  constructor(private mapservice:CommonmapService) { }

  ngOnInit(): void {
   this.coordinates=JSON.parse(localStorage.getItem('coordinates'))
   this.parcelname='';
   this.parcelarea='';
   this.parcelarea=localStorage.getItem('area')
   this.parcelname=localStorage.getItem('parcelname')
   console.log(this.parcelarea)
   console.log(this.parcelname)
   this.getgeolocations();
  }


  ngAfterViewInit() {
    MapLoaderService.load().then(() => {
      this.drawPolygon();
    });  
}

getgeolocations(){
  this.mapservice.getallgeolocations().subscribe(response=>{
    this.data = response
    console.log(response)
    let result = this.data.map((elem) => ({
      'lat': elem.lat,
      'lng': elem.lng
    }))
    let newresult = result.map(entry => 
      Object.entries(entry).reduce(
          (obj, [key, value]) => (obj[key] = parseFloat(value), obj), 
          {}
      )
    );
    this.dammycoordinates=JSON.stringify(newresult)
    this.dammymap=JSON.parse(this.dammycoordinates)
    // this.parceldtls=JSON.parse(JSON.stringify(response))
  })
}


drawPolygon(){
   this.map = new google.maps.Map(document.getElementById("map"), {
    zoom: 5,
    center: { lat: 24.886, lng: -70.268 },
    mapTypeId: "terrain"
  });

  this.drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.POLYLINE,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: ["polyline"]
    }
  });

  this.drawingManager.setMap(this.map);
    google.maps.event.addListener(
      this.drawingManager,
      "overlaycomplete",
      event => {
        if (event.type === google.maps.drawing.OverlayType.POLYLINE) {
          let areainhec = google.maps.geometry.spherical.computeArea(event.overlay.getPath().getArray())/10000
          console.log(areainhec)
        }
      }
    );

  // Define the LatLng coordinates for the polygon's path.
  const trianglestaticCoords = [
    { lat: 25.774, lng: -80.19 },
    { lat: 18.466, lng: -66.118 },
    { lat: 32.321, lng: -64.757 },
    { lat: 25.774, lng: -80.19 },
  ];
console.log(trianglestaticCoords)

  console.log(this.coordinates)
   const triangleCoords =this.coordinates
  const bermudaTriangle = new google.maps.Polygon({
    paths: triangleCoords,
    strokeColor: "#FF0000",
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: "#FF0000",
    fillOpacity: 0.35,
    text:"myparcel"
  });
  bermudaTriangle.setMap(this.map);
  var content = 'Parcel Name: ' + this.parcelname
  this.attachPolygonInfoWindow(bermudaTriangle,content)


}
attachPolygonInfoWindow(polygon,content) {
  var infoWindow = new google.maps.InfoWindow();
  google.maps.event.addListener(polygon, 'mouseover', function (e) {
      infoWindow.setContent(content);
      var latLng = e.latLng;
      infoWindow.setPosition(latLng);
      infoWindow.open(this.map);
  });
}




}






  // const triangleCoords: google.maps.LatLngLiteral[] = [
  //   { lat: 25.774, lng: -80.19 },
  //   { lat: 18.466, lng: -66.118 },
  //   { lat: 32.321, lng: -64.757 },
  // ];



//   console.log(this.dammymap)
//   const triangleCoords =this.dammymap
//  const bermudaTriangle = new google.maps.Polygon({
//    paths: triangleCoords,
//    strokeColor: "#FF0000",
//    strokeOpacity: 0.8,
//    strokeWeight: 2,
//    fillColor: "#FF0000",
//    fillOpacity: 0.35,
//  });
//  bermudaTriangle.setMap(map);
