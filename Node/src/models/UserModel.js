const {DataTypes} = require('sequelize');
const sequelize = require('../config/db').sequelize;
const {toHash} = require('../utilities/Password');

module.exports.UserModel=sequelize.define('User',{
    id:{
        type:DataTypes.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      gender: {
        type: DataTypes.STRING,
        allowNull:true
      },
      mobileNumber:{
          type:DataTypes.NUMBER,
          allowNull:true
      },
      emailId:{
          type:DataTypes.STRING,
          allowNull:true
      },
      designation:{
          type:DataTypes.STRING,
          allowNull:true
      },
      password:{
          type:DataTypes.STRING,
          allowNull:true
      }
},{
    freezeTableName:true,
    timestamps:false
});


