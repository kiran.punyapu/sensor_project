const express = require('express');
const router = express.Router();
const parcelModel = require('../models/parcelModel').parcelModel;
const locationModel = require('../models/geoLocation').locationModel;
router.post('/createparcel', (req, res) => {
    console.log(req.body)
    var parcelid;
    var object=[];
    const location = JSON.parse(req.body.location)
    console.log(location)
    parcelModel.create(req.body.parcelentry).then(pres => {
        parcelid = pres.id

            for (let i = 0; i < location.length; i++) {
                object.push({
                    "parcelid": pres.id,
                    "lat":location[i].lat,  
                    "lng":location[i].lng   
                })
            }
        locationModel.bulkCreate(object).then(lres=>{
            res.status(200).send(lres);  
        })
    });
});

router.get('/getall',async (req,res)=>{
    res.status(200).send(await parcelModel.findAll())
});

router.get('/getparbyid/:id',async (req,res)=>{
    res.status(200).send(await parcelModel.findAll({where:{id:req.params.id}}))
});


router.get('/getGeoLocation/:id',async (req,res)=>{
    res.status(200).send(await locationModel.findAll({where:{parcelid:req.params.id}}))
});

router.get('/findallgeo',async (req,res)=>{
    res.status(200).send(await locationModel.findAll())
});












// for(i=0;i<=location.length;i++){
    //     object ={
    //        "parcelid":pres.id,
    //        "lat":location[i].lat,
    //        "lng":location[i].lng
    //    }
    // } 



    // router.post('/signin',async (req,res)=>{
    //     const { emailId, password} = req.body;
    //     const user = await UserModel.findOne({where:{emailId:emailId}});
    //     if(!user) return res.status(200).send({message:"User Not found"});
    //     const userExists = user.get({plain:true});
    //     const isPassword = await comparePassword(userExists.password,password);
    //     if(!isPassword) return res.status(200).send({message:"Invalid Password"});
    //     let session=req.session;
    //     session.jwt= jwtSign(userExists)
    //     res.status(200).send({token:jwtSign(userExists)});  
    //  });


    // router.get('/getall',async (req,res)=>{
    //     res.status(200).send(await UserModel.findAll())
    // });

    // router.post("/getbyemail",async(req,res)=>{
    //    res.status(200).send(await (await UserModel.findOne({where:{emailId:req.body.emailid}})).get({plain:true}))
    // });
    // router.post("/deleteuser",async(req,res)=>{
    //     await UserModel.destroy({where:{id:req.body.id}});
    //     res.status(200).send({message:`User with ${req.body.id} deleted successfully`});
    // })

    module.exports = router;